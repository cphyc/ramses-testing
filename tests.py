import numpy as np
import yt

yt.funcs.mylog.setLevel(40)

###########################################
# Setup a few filters
###########################################


def processed_gas(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_tag']).astype(int).value > 0
    return ret
yt.add_particle_filter(
    'gas_tracer_processed', function=processed_gas, filtered_type='gas_tracer',
    requires=['particle_tag'])


def dead_star(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_family']).astype(int).value == 2
    return ret
yt.add_particle_filter(
    'dead_star', function=dead_star, filtered_type='io',
    requires=['particle_family']
)

###########################################
# Write your own tests here
#
# Any function named test_* will be runned
###########################################
