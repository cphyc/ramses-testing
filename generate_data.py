from glob import glob
import os
from subprocess import Popen, PIPE
import configparser
import sys

configs = glob('namelists/**/setup.ini', recursive=True)
if len(sys.argv) > 1:
    filters = sys.argv[1:]
    configs = [c for c in configs
               if any((f in c for f in filters))]
    print(configs)
    ok = input('OK? [Y/n]').lower().strip()
    if ok not in ['y', '']:
        print('OK, I\'m leaving :(')
        sys.exit(0)

for i, conf_file in enumerate(configs):
    config = configparser.ConfigParser()
    with open(conf_file, 'r') as f:
        config.read_file(f)

    ramses = os.path.join(os.getcwd(), 'bin', config['main']['ramses'])
    wd = os.path.split(conf_file)[0]

    # Execute ramses
    print('_'*80)
    print('Computing %s (%s/%s)' % (conf_file, i+1, len(configs)))
    MPIOPTS = os.environ.get('MPIOPTS', '')
    cmd = '/usr/bin/mpirun -np 2 %s %s namelist.nml' % (MPIOPTS, ramses)
    logfile = os.path.join(wd, 'log')

    print('Executing %s' % cmd)
    print('Saving log in %s' % logfile)
    print('_'*80)

    proc = Popen(cmd.split(), cwd=wd, stdout=PIPE)
    with open(logfile, 'w') as f:
        while proc.poll() is None:
            line = proc.stdout.readline().decode()
            if line:
                print(line, end='')
                f.write(line)
