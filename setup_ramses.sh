rm -rf ramses
git clone https://gitlab.com/cphyc/ramses.git
cwd=$(pwd)
cd ramses
git checkout master-tracer
cd trunk/ramses/bin

for target in Makefile Makefile.nfw Makefile.singlestar; do
    make clean
    make -f $target
done

cd $cwd
rm -rf bin
mkdir -p bin
cp ramses/trunk/ramses/bin/ramses_*3d bin
