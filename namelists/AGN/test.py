import yt
import numpy as np


def sink_tracer(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_family']) == 102
    return ret


yt.add_particle_filter('sink_tracer', function=sink_tracer, filtered_type='io',
                       requires=['particle_family'])


output0 = 'namelists/AGN/output_00001/info_00001.txt'
output1 = 'namelists/AGN/output_00002/info_00002.txt'


def test_sink_initial_props():
    '''Test initial position and velocity of sink'''
    ds = yt.load(output0)

    assert np.allclose(ds.r['sink', 'particle_position'].value, 0.5)
    assert np.allclose(ds.r['sink', 'particle_velocity'].value, 0.)


def test_sink_accretion():
    '''Accretion onto BH'''
    ds0 = yt.load(output0)
    ds1 = yt.load(output1)

    ds0.add_particle_filter('sink_tracer')
    ds1.add_particle_filter('sink_tracer')

    # Ensure there is one and only one sink
    assert len(ds0.r['sink', 'particle_mass']) == 1
    assert len(ds1.r['sink', 'particle_mass']) == 1

    # Ensure there is no tracer on the sink initally and there are some afer 1 timestep
    assert ds0.r['sink_tracer', 'particle_ones'].sum() == 0
    assert ds1.r['sink_tracer', 'particle_ones'].sum() > 0

    # Get initial mass of sink
    M0 = ds0.r['sink', 'particle_mass'].in_units('Msun')[0]

    eps = 0.1

    dM = ds1.r['sink', 'particle_mass'].in_units('Msun')[0] - M0
    dMtracer = ds1.r['sink_tracer', 'particle_mass'].in_units('Msun').sum()
    Ntracer = ds1.r['sink_tracer', 'particle_ones'].sum()

    var = 1 / np.sqrt(Ntracer)

    # Note: we have to take into account mass losses due to radiation
    assert dMtracer * (1-var) * (1-eps) < dM < dMtracer * (1+var) * (1-eps)


def test_sink_tracer_location():
    '''Tracers at same location as BH'''
    ds = yt.load(output1)

    ds.add_particle_filter('sink_tracer')

    # Load positions
    tracer_pos = ds.r['sink_tracer', 'particle_position']
    sink_pos = ds.r['sink', 'particle_position'][0]

    # Tracer at same position as sink?
    assert np.allclose(tracer_pos.value, sink_pos.value)
