import numpy as np
import yt
yt.funcs.mylog.setLevel(40)


def processed_gas(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_tag']).astype(int).value > 0
    return ret
yt.add_particle_filter(
    'gas_tracer_processed', function=processed_gas, filtered_type='gas_tracer',
    requires=['particle_tag'])


def dead_star(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_family']).astype(int).value == 2
    return ret
yt.add_particle_filter(
    'dead_star', function=dead_star, filtered_type='io',
    requires=['particle_family']
)


#########################################
# Testing blob
#########################################
blob_test1 = 'namelists/blob/output_00001/info_00001.txt'
blob_tests = 'namelists/blob/output_?????/info_?????.txt'
blob_testlast = 'namelists/blob/output_00016/info_00016.txt'


def test_particle_move():
    ts = yt.load(blob_tests)
    ds = yt.load(blob_testlast)

    ds = ts[0]
    indices = ds.r['gas_tracer', 'particle_identity']
    trajs = ts.particle_trajectories(
        indices,
        fields=['particle_position_%s' % k for k in 'xyz'],
        suppress_logging=True)

    # Check at least one particle has moved each timestep
    def _test_traj(i):
        '''Particle move %s''' % ts[i]
        for k in 'xyz':
            assert any(trajs['particle_position_%s' % k][:, i] !=
                       trajs['particle_position_%s' % k][:, i+1])

    for i in range(len(ts)-1):
        yield _test_traj, i


def test_particle_location():
    '''Particles at the center of cells.'''
    ds = yt.load(blob_testlast)
    pos = ds.r['particle_position'].in_units('code_length').value
    levelmax = 8
    pos = pos * 2**(levelmax+1)

    assert np.allclose(pos, np.floor(pos))
