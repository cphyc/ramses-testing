import yt
import numpy as np

_PLOTS = False

if _PLOTS:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

yt.enable_plugins()


inplace_init = 'namelists/inplace_init/output_00001/info_00001.txt'


def test_mass():
    '''Check the initialized mass is equal to total mass.'''
    ds = yt.load(inplace_init)

    ad = ds.all_data()
    gmass, tmass = ad.quantities.total_mass().in_units('code_mass')
    nparts = ds.r['gas_tracer', 'particle_ones'].sum().value

    print(nparts)
    print(gmass, tmass)

    dm = 1/np.sqrt(nparts) * tmass

    assert (gmass < tmass+dm)
    assert (gmass > tmass-dm)


def spatial_distribution():
    '''Check that tracers are correctly spread in space.'''
    ds = yt.load(inplace_init)

    Ntracer = ds.r['gas_tracer', 'particle_ones'].sum().value
    m = ds.r['cell_mass']

    Mtracer = m.sum() / Ntracer

    expected = m / Mtracer

    assert np.allclose()


if __name__ == '__main__':
    test_mass()
