import yt
import numpy as np

_PLOTS = False

if _PLOTS:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

yt.enable_plugins()


sedov = 'namelists/sedov3d/output_00011/info_00011.txt'
# sedov = 'output_00011/info_00011.txt'
def test_blast():
    '''Radial profiles should match for Sedov blast'''
    ds = yt.load(sedov)

    ad = ds.all_data()
    prof = yt.create_profile(ad, ['radius'],
                             fields=['density', 'gas_tracer_density', 'ones'])

    x = prof.x
    y1 = prof['density'].value
    y2 = prof['gas_tracer_density'].value

    m = (y1 > 0) & (y2 > 0) & (x > ds.quan(0.2, 'cm'))

    dy = (y2 - y1) / y1

    if _PLOTS:
        plt.cla()
        plt.plot(prof.x[m], dy[m]*100)
        plt.xlabel(r'x [cm]')
        plt.ylabel(r'$(\rho_{gas} - \rho_{tracer}) / \rho_{gas}$ ($\%$)')
        plt.tight_layout()
        plt.savefig('radial_profile.pdf')


    # Allow wiggle of at most 1% at 0.2cm and larger
    assert all(dy[m] > -0.01)
    assert all(dy[m] < 0.01)


def test_isotropy():
    '''The tracer distribution should be isotropic'''
    ds = yt.load(sedov)
    pos = ds.r['gas_tracer', 'particle_position']

    xbins = np.linspace(0, 1, 33)
    hists = [np.histogram(pos[:, idim], bins=xbins, normed=True)[0] for idim in range(3)]

    def _check_dir(idir):
        '''Checks that the histogram goes above 1.01 (at blast radius)
        and below 0.99 (below radius).

        We use a normed histogram so that the test does not depend on the number of particles.'''
        assert any(hists[idir] > 1.005)
        assert any(hists[idir] < 0.99)

    if _PLOTS:
        plt.cla()
        for h in hists:
            plt.plot(xbins[1:], h)

        plt.savefig('histograms.pdf')

    # Check that the blast has a significance of more than 3% in each direction
    for i in range(3):
        yield _check_dir, i
