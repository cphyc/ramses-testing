import numpy as np
import yt
from scipy.optimize import curve_fit


no_tracer_outputs = 'namelists/simple_advection/no_tracer/output_?????/info_?????.txt'
tracer_outputs = 'namelists/simple_advection/tracer/output_?????/info_?????.txt'

def test_hydro_equality():
    ts0 = yt.load(no_tracer_outputs)
    ts1 = yt.load(tracer_outputs)

    def _check_em(ds0, ds1):
        '''Hydro output independent of tracers %s''' % ds0
        assert np.isclose(ds0.current_time.value, ds1.current_time.value)
        dens0 = ds0.r['density'].value
        dens1 = ds1.r['density'].value

        assert np.allclose(dens0, dens1)

    for ds0, ds1 in zip(ts0, ts1):
        yield _check_em, ds0, ds1


tracer_output_last = 'namelists/simple_advection/tracer/output_00011/info_00011.txt'
def test_tracer_distribution():
    '''Tracer distribution ~ gas distribution'''
    ds = yt.load(tracer_output_last)
    ad = ds.all_data()
    N = 2**3 + 1

    x = ds.r['x']
    y = ds.r['y']
    m = ds.r['cell_mass']

    xp = ds.r['particle_position_x']
    yp = ds.r['particle_position_y']
    mp = ds.r['particle_mass']

    bins = np.linspace(0, 1, N)
    M, *_ = np.histogram2d(x, y, weights=m, bins=bins)
    Mp, *_ = np.histogram2d(xp, yp, weights=mp, bins=bins)
    Np, *_ = np.histogram2d(xp, yp, bins=bins)

    x = M.flatten()
    y = Mp.flatten()
    dy = y / np.sqrt(Np.flatten())

    logx, logy = np.log10(x), np.log10(y)

    curve = lambda x, a: a*x
    popt, pcov = curve_fit(curve, logx, logy, p0=[1])

    assert np.isclose(popt[0], 1.)
