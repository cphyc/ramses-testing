import numpy as np
import yt

#########################################
# Setup a few filters
#########################################


def processed_gas(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_tag']).astype(int).value > 0
    return ret
yt.add_particle_filter(
    'gas_tracer_processed', function=processed_gas, filtered_type='gas_tracer',
    requires=['particle_tag'])


def dead_star(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_family']).astype(int).value == 2
    return ret
yt.add_particle_filter(
    'dead_star', function=dead_star, filtered_type='io',
    requires=['particle_family']
)


def inPoisson(a, b, N):
    amin = a * (1-1/np.sqrt(N))
    amax = a * (1+1/np.sqrt(N))
    if amin > b or amax < b:
        print('This should be true %.2f < %.2f < %.2f' % (amin, b, amax))
        return False

    return True


#########################################
# Testing star formation + feedback with AMR
#########################################
SF_all = 'namelists/single_star_formation_AMR/output_?????/info_?????.txt'
SF_first = 'namelists/single_star_formation_AMR/output_00002/info_00002.txt'


def test_star_tracer_location():
    def _test_one(ds):
        '''Star tracers at location of stars (AMR) %s''' % ds
        # Skip datasets with no star
        if len(ds.r['star', 'particle_ones']) == 0:
            return

        # Ensure there is only one star
        assert len(ds.r['star', 'particle_ones']) == 1

        # Ensure all the tracers are located where the star is
        assert np.allclose(ds.r['star_tracer', 'particle_position'].value,
                           ds.r['star', 'particle_position'][0].value)

        # Ensure the star tracer point at the right value
        assert np.all(ds.r['star_tracer', 'particle_partp'].value ==
                      ds.r['star', 'particle_identity'][0].value)

    ts = yt.load(SF_all)

    for ds in ts:
        yield _test_one, ds


def test_star_formation_mass():
    '''SF attaches right number of tracers (AMR)'''
    ds = yt.load(SF_first)

    tracer_mass = ds.r['star_tracer', 'particle_mass'].sum().value
    tracer_N = ds.r['star_tracer', 'particle_ones'].sum().value
    star_mass = ds.r['star', 'particle_mass'].sum().value

    assert inPoisson(tracer_mass, star_mass, tracer_N)


class testSNEvent():
    iFirst = None

    def setUp(self):
        ts = yt.load(SF_all)

        iFirst = self.iFirst
        if iFirst is not None:
            self.SF_explosion_before = ts[iFirst-1]
            self.SF_explosion_after = ts[iFirst]
            return

        for i, ds in enumerate(ts):
            ds.add_particle_filter('gas_tracer_processed')
            if ds.r['gas_tracer_processed', 'particle_ones'].sum() > 0:
                self.iFirst = i
                self.SF_explosion_after = ds
                break
            self.SF_explosion_before = ds

    def tearDown(self):
        pass

    def test_star_explosion_quantity(self):
        '''SN explosion has right amount of tracers (AMR)'''
        ds0 = self.SF_explosion_before
        ds1 = self.SF_explosion_after

        for ds in [ds0, ds1]:
            ds.add_particle_filter('gas_tracer_processed')
            ds.add_particle_filter('dead_star')

        # Check there is no gas tracer released 0 the output
        assert ds0.r['gas_tracer_processed', 'particle_ones'].sum() == 0
        assert ds1.r['gas_tracer_processed', 'particle_ones'].sum() > 0

        # Check the amount of gas released is similar to dM*
        Nstar_tracer0 = ds0.r['star_tracer', 'particle_ones'].sum().value
        Nstar_tracer = ds1.r['star_tracer', 'particle_ones'].sum().value
        Nreleased = ds1.r['gas_tracer_processed', 'particle_ones'].sum().value

        eta_sn = 0.5  # This is the eta from the namelist!

        assert inPoisson(Nstar_tracer, Nstar_tracer0 * (1-eta_sn), Nstar_tracer)
        assert inPoisson(Nreleased, Nstar_tracer0 * (eta_sn), Nreleased)



# Cannot check for the distribution as the tracers get projected into the nearby cells
