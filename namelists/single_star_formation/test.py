import numpy as np
import yt
from nose import with_setup

#########################################
# Setup a few filters
#########################################


def processed_gas(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_tag']).astype(int).value > 0
    return ret
yt.add_particle_filter(
    'gas_tracer_processed', function=processed_gas, filtered_type='gas_tracer',
    requires=['particle_tag'])


def dead_star(pfilter, data):
    ret = (data[pfilter.filtered_type, 'particle_family']).astype(int).value == 2
    return ret
yt.add_particle_filter(
    'dead_star', function=dead_star, filtered_type='io',
    requires=['particle_family']
)


def inPoisson(a, b, N):
    amin = a * (1-1/np.sqrt(N))
    amax = a * (1+1/np.sqrt(N))
    if amin > b or amax < b:
        print('This should be true %.2f < %.2f < %.2f' % (amin, b, amax))
        return False

    return True


#########################################
# Testing star formation + feedback no AMR
#########################################
SF_all = 'namelists/single_star_formation/output_?????/info_?????.txt'
SF_first = 'namelists/single_star_formation/output_00002/info_00002.txt'


def test_star_tracer_location():
    def _test_one(ds):
        '''Star tracers at location of stars %s''' % ds
        # Skip datasets with no star
        if len(ds.r['star', 'particle_ones']) == 0:
            return

        # Ensure there is only one star
        assert len(ds.r['star', 'particle_ones']) == 1

        # Ensure all the tracers are located where the star is
        assert np.allclose(ds.r['star_tracer', 'particle_position'].value,
                           ds.r['star', 'particle_position'][0].value)

        # Ensure the star tracer point at the right value
        assert np.all(ds.r['star_tracer', 'particle_partp'].value ==
                      ds.r['star', 'particle_identity'][0].value)

    ts = yt.load(SF_all)

    ok = False
    for ds in ts:
        yield _test_one, ds


def test_star_formation_mass():
    '''SF attaches right number of tracers'''
    # Find first output that creates a
    ds = yt.load(SF_first)

    tracer_mass = ds.r['star_tracer', 'particle_mass'].sum().value
    tracer_N = ds.r['star_tracer', 'particle_ones'].sum().value
    star_mass = ds.r['star', 'particle_mass'].sum().value

    assert np.isclose(tracer_mass, star_mass, rtol=0.05)

class testSNEvent():
    iFirst = None

    def setUp(self):
        ts = yt.load(SF_all)

        iFirst = self.iFirst
        if iFirst is not None:
            self.SF_explosion_before = ts[iFirst-1]
            self.SF_explosion_after = ts[iFirst]
            self.SF_explosion_later = ts[iFirst+1]
            return

        for i, ds in enumerate(ts):
            ds.add_particle_filter('gas_tracer_processed')
            if ds.r['gas_tracer_processed', 'particle_ones'].sum() > 0:
                self.iFirst = i
                self.SF_explosion_after = ds
                self.SF_explosion_later = ts[i+1]
                break
            self.SF_explosion_before = ds

    def tearDown(self):
        pass

    def test_star_explosion_quantity(self):
        '''SN explosion has right amount of tracers'''
        ds0 = self.SF_explosion_before
        ds1 = self.SF_explosion_after

        for ds in [ds0, ds1]:
            ds.add_particle_filter('gas_tracer_processed')
            ds.add_particle_filter('dead_star')

        # Check there is no gas tracer released 0 the output
        assert ds0.r['gas_tracer_processed', 'particle_ones'].sum() == 0
        assert ds1.r['gas_tracer_processed', 'particle_ones'].sum() > 0

        # Check the amount of gas released is similar to dM*
        Nstar_tracer0 = ds0.r['star_tracer', 'particle_ones'].sum().value
        Nstar_tracer = ds1.r['star_tracer', 'particle_ones'].sum().value
        Nreleased = ds1.r['gas_tracer_processed', 'particle_ones'].sum().value

        eta_sn = 0.5  # This is the eta from the namelist!

        assert inPoisson(Nstar_tracer, Nstar_tracer0 * (1-eta_sn), Nstar_tracer / 4)
        assert inPoisson(Nreleased, Nstar_tracer0 * (eta_sn), Nreleased / 4)


    def test_star_explosion_spatial(self):
        '''Tracers are released in the 48 neighbor cells'''
        ds = self.SF_explosion_after
        ds.add_particle_filter('gas_tracer_processed')
        ds.add_particle_filter('dead_star')

        # star_pos = ds.r['dead_star', 'particle_position'].value[0]
        pos = ds.r['gas_tracer_processed', 'particle_position'].value

        uniq, counts = np.unique(pos, axis=0, return_counts=True)

        # Be sure there's particle in each box
        assert len(uniq) == 48

        # Be sure there's a poisson distrib for the particles
        nincell_expected = counts.sum() / 48
        p = 1/48

        # The number of particles in each box is given by a binomial distrib
        N = counts.sum()
        var = N * p * (1-p)
        sig = np.sqrt(var)
        for nincell in counts:
            # We check that the particles end up in boxes with no more
            # than 4 sigma fluctuations. Why 4? Donnow
            assert nincell_expected-4*sig < nincell < nincell_expected+4*sig

    def test_star_explosion_after(self):
        '''Tracer particles are moved to the grid after one step'''
        ds = self.SF_explosion_later

        ds.add_particle_filter('gas_tracer_processed')

        pos = ds.r['gas_tracer_processed', 'particle_position'].in_units('code_length').value
        levelmax = 5
        pos = pos * 2**(levelmax) + 0.5

        assert np.allclose(pos, np.floor(pos))
