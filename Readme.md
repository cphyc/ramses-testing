# Testing RAMSES

This testing suit is made of 3 steps:
1. Setup a fresh install of RAMSES.
2. Generate data.
3. Run some (python) test on the result of the data.

## Setup
First, clone the repository then run
```
bash setup_ramses.sh
```
You may have to adapt the script to clone from the right git repository as well as the right branch. Also don't hesitate to modify the `Makefile`s to suit your compiler etc...
At the end, the script copies the created `ramses` binaries into the `bin` folder.

## Generate data
Simply run
```
python generate_data.py
```
The script will walk into any subdirectory of `namelists` that contain a file named `setup.ini` and will execute ramses in the folder with the namelist `namelist.nml`.

## Running test
If you don't have it, you'll need to install nose as well as yt
```
pip install nose yt
```
To run the tests, run:
```
nosetests -v
```

# Adding a test
If you wish to add a test, create a new folder in the `namelists` folder with the name of your test. In this folder create a `config.ini` file that contains
```
[main]
ramses = name_of_the_ramses_executable
```
where `name_of_the_ramses_executable` matches the name of one of the file in `bin`. Also add a `namelist.nml` along with any data relevant to the execution of your test.

To write the actual tests, either append your tests to `tests.py` or create `namelists/yourtest/test.py`. See e.g. `namelists/blob/test.py`.

You can use `example/namelists/example` as a starting point. Just copy `example/namelists/example` to `namelists/your_test` and adapt the files to match your requirements.
